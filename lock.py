import os, cv2
import pyscreenshot
import numpy as np
from PIL import Image

path = os.environ['HOME']+'/Pictures/'

if not os.path.exists(path):  #if the folder does not exist than create it
    os.mkdir(path)

pyscreenshot.grab_to_file(path+'screen.png') #take a screenshot and save in Pictures

img = cv2.imread(path+'screen.png')
h, w = (Image.open(path+'screen.png')).size
image = Image.open(path+'screen.png').load()

def get_avg(x, y, n, m, image):
	r, g, b = 0, 0, 0
	count = 0
	for s in range(x, n):
	    for t in range(y, m):
	        pixlr, pixlg, pixlb = image[s, t]
	        r += pixlr
	        g += pixlg
	        b += pixlb
	        count += 1
	        #print(r, pixlr, count)
	return ((r/count), (g/count), (b/count))

r, g, b = get_avg(int((w/2)-100),int((h/2)-100), int((w/2)+100), int((h/2)+100), img)
#r, g, b = get_avg(0, 0, w, h,img)
#print(r, g, b)

# cv2.rectangle(img,(int((1920/2)-50),int((1080/2)-50)),(int((1920/2)+50),int((1080/2)+50)),(0,255,0),3)

# np_image_average = np.average(cv2.imread(path+'screen_blur.png'))
blur = cv2.blur(cv2.imread(path+'screen.png'), (25, 25)) #bluring the image, high number means bigh blur square

cv2.imwrite(path+'screen_blur.png', blur) #rewrite the previous blured image

os.popen('rm '+path+'screen.png')#remove no-blured image

img = cv2.imread(path+'screen_blur.png')

#following lines add text on the blured image 
x1 = 920
x2 = 890
x3 = 895
y1 = 500
y2 = 550
y3 = 600

cv2.imwrite(path+'screen_blur.png', cv2.putText(np.asarray(cv2.imread(path+'screen_blur.png')),'Type', (x1,y1), cv2.FONT_HERSHEY_TRIPLEX, 1, (255-r,255-g,255-b), 2)) 
cv2.imwrite(path+'screen_blur.png', cv2.putText(np.asarray(cv2.imread(path+'screen_blur.png')),'password', (x2, y2), cv2.FONT_HERSHEY_TRIPLEX, 0.9, (255-r,255-g,255-b), 2))
cv2.imwrite(path+'screen_blur.png', cv2.putText(np.asarray(cv2.imread(path+'screen_blur.png')),'to unlock', (x3, y3), cv2.FONT_HERSHEY_TRIPLEX, 0.85, (255-r,255-g,255-b), 2))

# cv2.rectangle(img,(int((1920/2)-50),int((1080/2)-50)),(int((1920/2)+50),int((1080/2)+50)),(0,255,0),3)
# cv2.imwrite(path+'screen_blur.png', cv2.rectangle(img,(int((1920/2)-100),int((1080/2)-100)),(int((1920/2)+100),int((1080/2)+100)),(255-r,255-g,255-b),3))

os.popen("i3lock -n  --image="+path+'screen_blur.png'+' --pointer=win|default -e -f' ) #lock the screen
#os.popen("xdg-screensaver lock")
